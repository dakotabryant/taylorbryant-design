import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import media from "styled-media-query";
import hands from "../assets/images/hands.png";
import linkedIn from "../assets/images/Social/linkedin.svg";
import behance from "../assets/images/Social/behance.svg";

const FooterContainer = styled.footer`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 150px 0;
  position: relative;
  img {
    width: 100%;
    margin-top: 60px;
  }
  ${media.lessThan("large")`
    padding: 100px 0;
  `}
`;

const HeadingText = styled.h3`
  font-family: ${props => props.theme.fonts.primary};
  text-transform: uppercase;
  font-size: 72px;
  line-height: 46px;
  font-weight: 800;
  position: relative;
  ${media.lessThan("large")`
    text-align: center;
    font-size: 62px;
    line-height: 62px;
  `}
  &:after {
    content: "";
    display: inline-block;
    background-color: ${props => props.theme.green};
    height: 14px;
    width: 285px;
    position: absolute;
    top: 40%;
    right: 15px;
    z-index: -1;
    ${media.lessThan("large")`
    top: 70%;
    right: 65px;
  `}
  }
`;

const SubText = styled.p`
  font-family: ${props => props.theme.fonts.secondary};
  font-weight: ${props => (props.isBold ? "800" : "400")};
  margin-top: ${props => (props.isBold ? "33px" : "60px")};
  font-size: 18px;
  a {
    color: black;
  }
  ${media.lessThan("large")`
      text-align: center;
      padding: 0 15px;
      margin-top: 40px;
  `}
`;

const ContactDetails = styled.div`
  margin: 0 auto;
  width: calc(100% - 60px);
  border-top: 3px solid black;
  padding: 30px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${media.lessThan("large")`
      flex-direction: column;
      text-align: center;
  `}
  h3 {
    color: ${props => (props.isWhite ? "white" : "black")};
    font-size: 24px;
    font-family: "Montserrat", sans-serif;
    font-weight: 800;
    text-transform: uppercase;
  }
`;

const LinksContainer = styled.div`
  display: flex;
  justify-content: space-around;
  font-family: ${props => props.theme.fonts.secondary};
  ${media.lessThan("large")`
      flex-direction: column;
      align-items: center;
  `}
  a {
    color: black;
    text-decoration: none;
    font-weight: 700;
    margin: 0 25px;
    ${media.lessThan("large")`
      flex-direction: column;
      align-items: center;
      margin: 20px 0;
  `}
    &:last-of-type {
      margin-right: 0;
    }
  }
`;

const SocialContainer = styled.div`
  display: flex;
  justify-content: space-around;
  position: absolute;
  left: 50%;
  ${media.lessThan("large")`
      position: initial;
      margin-top: 20px;
  `}
  img {
    margin: 0 10px;
  }
`;

const Footer = () => {
  return (
    <>
      <FooterContainer id="contact">
        <HeadingText>Get In Touch.</HeadingText>
        <SubText>
          I am currently open to new opportunities. Want to chat? Drop me a
          line.
        </SubText>
        <SubText isBold>
          <a href="mailto:mrstaylorbryant@gmail.com">
            mrstaylorbryant@gmail.com
          </a>
        </SubText>
        <img src={hands} alt="" />
      </FooterContainer>

      <ContactDetails>
        <Link to="/">
          <h3>Taylor Bryant</h3>
        </Link>
        <SocialContainer>
          <a
            target="_blank"
            href="https://www.linkedin.com/in/taylor-s-bryant/"
          >
            <img src={linkedIn} alt="LinkedIn - Taylor Bryant" />
          </a>
          <a target="_blank" href="https://www.behance.net/taylor-bryant">
            <img src={behance} alt="Behance - Taylor Bryant" />
          </a>
        </SocialContainer>
        <LinksContainer>
          <Link to="/">home</Link>
          <Link to="/work">work</Link>
          <Link to="/about">about</Link>
          <a href="#contact">contact</a>
        </LinksContainer>
      </ContactDetails>
    </>
  );
};

export default Footer;
