import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { useState } from "react";
import styled, { keyframes, css } from "styled-components";
import media from "styled-media-query";

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 0.85;
  }
`;

const fadeOut = keyframes`
  from {
    opacity: .85;
  }

  to {
    opacity: 0;
  }
`;

const HeaderContainer = styled.header`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 30px;
  height: 80px;
  ${media.lessThan("large")`
    padding: 15px;
  `}
  button,
  a {
    z-index: 1000;
  }
  h3 {
    color: ${props => (props.isWhite ? "white" : "black")};
    font-size: 24px;
    font-family: "Montserrat", sans-serif;
    font-weight: 800;
    text-transform: uppercase;
  }
`;

const NavContainer = styled.nav`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  z-index: 100;
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100vw;
  background-color: rgba(255, 255, 255, 0.85);
  visibility: ${props => (props.isActive ? "visible" : "hidden")};
  animation: ${props => (props.isActive ? fadeIn : fadeOut)} 0.25s linear;
  transition: visibility 1s linear;
  a {
    ${({ theme, isActive }) => css`
      position: relative;
      font-family: ${props => props.theme.fonts.primary};
      opacity: 1;
      font-size: 160px;
      font-weight: 800;
      color: black;
      -webkit-text-fill-color: transparent; /* Will override color (regardless of order) */
      -webkit-text-stroke-width: 4px;
      -webkit-text-stroke-color: black;
      ${media.lessThan("large")`
      font-size: 66px;
      -webkit-text-fill-color: black; /* Will override color (regardless of order) */
      -webkit-text-stroke-width: 0;

      `}
      &:hover {
        -webkit-text-fill-color: black; /* Will override color (regardless of order) */
        -webkit-text-stroke-width: 0;
        -webkit-text-stroke-color: transparent;
        &::before {
          z-index: -1;
          display: inline-block;
          position: absolute;
          top: 40%;
          left: 0;
          content: "";
          width: 100%;
          height: 40px;
          background-color: ${theme.green};
        }
      }
    `}
  }
`;

const Header = ({ siteTitle, isWhite }) => {
  const [isActive, setActive] = useState(false);
  return (
    <HeaderContainer isWhite={isWhite}>
      <Link to="/">
        <h3>Taylor Bryant</h3>
      </Link>
      <button
        className={`hamburger hamburger--emphatic ${
          isActive ? "is-active" : ""
        }`}
        type="button"
        onClick={() => setActive(!isActive)}
      >
        <span className="hamburger-box">
          <span className="hamburger-inner"></span>
        </span>
      </button>
      {isActive && (
        <NavContainer isActive={isActive}>
          <Link to="/">home</Link>
          <Link to="about">about</Link>
          <Link to="work">work</Link>
          <Link to="#contact" onClick={() => setActive(false)}>
            contact
          </Link>
        </NavContainer>
      )}
    </HeaderContainer>
  );
};

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
