import React from "react";
import styled from "styled-components";
import media from "styled-media-query";
import gargoyle from "../../assets/images/gargoyle.png";
const AboutContainer = styled.section``;

const StyledHeader = styled.h3`
  position: relative;
  text-transform: uppercase;
  font-size: 42px;
  padding-left: 10%;
  margin-top: 200px;
  ${media.lessThan("large")`
    padding-left: 15px;
    width: 100%;
    font-size: 32px;
    margin-top: 100px;
  `}
  &:after {
    content: "";
    display: inline-block;
    background-color: ${props => props.theme.green};
    height: 14px;
    width: 100%;
    position: absolute;
    bottom: 18px;
    left: -46%;
    z-index: -1;
  }
`;

const DescriptionSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 100px;
  ${media.lessThan("large")`
    flex-direction: column-reverse;
  `}
  img {
    max-width: 600px;
    ${media.lessThan("large")`
      max-width: 80%;
      margin-left: auto;
      margin-right: -50px;
  `}
  }
  p {
    padding-top: 260px;
    margin: 0 auto;
    font-size: 18px;
    line-height: 24px;
    max-width: 500px;
    font-family: ${props => props.theme.fonts.secondary};
    ${media.lessThan("large")`
    padding-top: 60px;
    margin-left: 0;
    padding-left: 15px;
    width: 100%;
  `}
    a {
      font-size: 20px;
      position: relative;
      font-family: ${props => props.theme.fonts.secondary};
      font-weight: 700;
      color: black;
      &:after {
        content: "";
        display: inline-block;
        background-color: ${props => props.theme.green};
        height: 14px;
        width: 100%;
        position: absolute;
        bottom: 5px;
        left: 0;
        z-index: -1;
      }
    }
  }
`;

const AboutText = styled.h2`
  font-size: 240px;
  color: #efefef;
  position: absolute;
  right: 0;
  z-index: -1;
  ${media.lessThan("large")`
    font-size: 120px;
    right: -5%;
    top: 45%;
  `}
`;

const Hero = () => {
  return (
    <AboutContainer>
      <StyledHeader>
        I'm a multidisciplinary
        <br /> designer with a passion for
        <br /> great user experiences.
      </StyledHeader>
      <AboutText>ABOUT</AboutText>
      <DescriptionSection>
        <p>
          Hi! I’m Taylor. Creator of pretty things, lover of puppies and color
          coordination. <br /> <br /> My background ranges from web design to
          print design, project managment to account management, which gives me
          a unique skill set for any project I tackle. However, design has
          always been my true passion. I am well-versed in Adobe Creative Suite
          and Sketch, and also have knowledge in HTML and CSS. Details and
          organization is my thing, so I try to make sure all of my work is
          thought out and done to the best of my ability. <br /> <br />
          Feel free to check out my work and previous experience.
          <br /> <br />
          <a href="#contact">let's chat →</a>
        </p>
        <img src={gargoyle} alt="" />
      </DescriptionSection>
    </AboutContainer>
  );
};

export default Hero;
