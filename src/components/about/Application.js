import React from "react";
import styled from "styled-components";
import media from 'styled-media-query';

const ApplicationContainer = styled.div`
  display: flex;
  margin-bottom: 25px;
  padding-left: 145px;
  ${media.lessThan("large")`
    padding-left: 0;
  `}
  h5 {
    font-weight: 400;
    font-size: 18px;
    font-family: ${props => props.theme.fonts.secondary};
    margin-right: auto;
  }
`;

const Applications = ({ application: { name, rating } }) => {
  return (
    <>
      <ApplicationContainer>
        <h5>{name}</h5>
      </ApplicationContainer>
    </>
  );
};

export default Applications;
