import React from "react";
import styled from "styled-components";

const StyledAttribute = styled.div`
  position: relative;
  font-family: ${props => props.theme.fonts.secondary};
  font-size: 18px;
  margin-bottom: 10px;
  line-height: 24px;
  &:before {
    font-size: 30px;
    font-weight: 700;
    content: "+";
    color: ${props => props.theme.green};
    position: absolute;
    top: -2px;
    left: -30px;
  }
`;

const Attribute = ({ attribute }) => {
  return <StyledAttribute>{attribute}</StyledAttribute>;
};

export default Attribute;
