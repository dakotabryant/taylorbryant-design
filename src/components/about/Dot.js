import React from "react";
import styled from "styled-components";
import media from "styled-media-query";

const DotContainer = styled.div`
  display: flex;
  margin-left: 150px;
  margin-bottom: 25px;
  ${media.lessThan("large")`
    margin-left: 0;
  `};
`;

const StyledDot = styled.span`
  background-color: ${props => (props.isGreen ? props.theme.green : "#F0F0F0")};
  height: 10px;
  width: 10px;
  border-radius: 100%;
  margin: 5px;
`;

const renderDots = rating => {
  let dots = [];
  for (let i = 0; i < 10; i++) {
    const isGreen = i < rating;
    dots.push(<StyledDot isGreen={isGreen}></StyledDot>);
  }
  return dots;
};
const Dot = ({ application: { rating } }) => {
  return <DotContainer>{rating && renderDots(rating)}</DotContainer>;
};

export default Dot;
