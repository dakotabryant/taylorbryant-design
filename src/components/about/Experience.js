import React from "react";
import styled from "styled-components";
import media from "styled-media-query";
import Job from "./Job";
import workHistory from "../../constants/workExperience";
import resume from "../../assets/resume.pdf";

const ExperienceContainer = styled.section`
  margin: 0 auto;
  margin-top: 150px;
  display: flex;
  align-items: center;
  flex-direction: column;
  max-width: 1100px;
  ${media.lessThan("large")`
    margin-top: 75px;
  `}
  h3 {
    font-size: 42px;
    text-transform: uppercase;
  }
`;
const JobsContainer = styled.div`
  margin-top: 100px;
`;

export const StyledResumeLink = styled.div`
  margin-top: 50px;
  ${media.lessThan("large")`
    text-align: center;
  `}
  a {
    font-size: 20px;
    position: relative;
    font-family: ${props => props.theme.fonts.secondary};
    font-weight: 700;
    color: black;
    &:after {
      content: "";
      display: inline-block;
      background-color: ${props => props.theme.green};
      height: 14px;
      width: 100%;
      position: absolute;
      bottom: 5px;
      left: 0;
      z-index: -1;
    }
  }
`;

const Experience = () => {
  return (
    <ExperienceContainer>
      <h3>Experience</h3>
      <StyledResumeLink>
        <a href={resume} target="_blank">
          download full resume
        </a>
      </StyledResumeLink>
      <JobsContainer>
        {workHistory &&
          workHistory.map(job => <Job key={job.title} job={job}></Job>)}
      </JobsContainer>
    </ExperienceContainer>
  );
};

export default Experience;
