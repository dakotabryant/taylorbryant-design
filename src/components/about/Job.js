import React from "react";
import styled, { css } from "styled-components";
import media from "styled-media-query";

const JobContainer = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 50px;
  ${media.lessThan("large")`
    flex-direction: column;
    align-items: center;
  `}
`;

const DateContainer = styled.div`
  ${({ theme }) => css`
    font-family: ${theme.fonts.secondary};
    background-color: ${theme.green};
    padding: 8px 15px;
    width: 270px;
    text-align: center;
    font-size: 18px;
    font-weight: 700;
    margin-right: 50px;
  `}
  ${media.lessThan("large")`
    margin-right: 0;
    width: calc(100% - 30px);
  `}
`;

const DetailsContainer = styled.div`
  max-width: 65%;
  ${media.lessThan("large")`
    text-align: center;
    max-width: calc(100% - 30px);
  `}
`;

const Title = styled.h4`
  font-size: 24px;
  text-transform: uppercase;
  ${media.lessThan("large")`
    margin-top: 20px;
  `}
`;

const CompanyCityTitle = styled.h5`
  font-family: ${props => props.theme.fonts.secondary};
  font-size: 18px;
  font-weight: 400;
  margin-top: 10px;
`;

export const TagsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 10px;
  text-transform: uppercase;
  margin-top: 20px;
  .box {
    background-color: #f0f0f0;
    padding: 2.5px 10px;
    margin: 5px;
  }
  ${media.lessThan("large")`
    justify-content: center;
  `} p {
    letter-spacing: 1.67px;
  }
`;

const Job = ({ job: { title, company, cityState, date, tags } }) => {
  return (
    <JobContainer>
      <DateContainer>{date}</DateContainer>
      <DetailsContainer>
        <Title>{title}</Title>
        <CompanyCityTitle>
          {company} | {cityState}
        </CompanyCityTitle>
        <TagsContainer>
          {tags &&
            tags.map(tag => (
              <div className="box">
                <p>{tag}</p>
              </div>
            ))}
        </TagsContainer>
      </DetailsContainer>
    </JobContainer>
  );
};

export default Job;
