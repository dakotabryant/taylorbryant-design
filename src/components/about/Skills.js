import React from "react";
import styled from "styled-components";
import media from "styled-media-query";
import { attributes, applications } from "../../constants/skills";
import Application from "./Application";
import Dot from "./Dot";
import Attribute from "./Attribute";
import { StyledResumeLink } from "./Experience";
import resume from "../../assets/resume.pdf";

const SkillsContainer = styled.div`
  margin-top: 150px;
  ${media.lessThan("large")`
    margin-top: 25px;
  `}
  h3 {
    text-transform: uppercase;
    text-align: center;
    font-size: 42px;
    margin-bottom: 75px;
  }
`;
const SkillsAndAbilities = styled.div`
  display: flex;
  justify-content: center;
  ${media.lessThan("large")`
    flex-wrap: wrap;
    padding: 0 15px;
  `}
`;

const ApplicationsContainer = styled.div`
  ${media.lessThan("large")`
    width: 50%;
  `}
`;

const AbilitiesContainer = styled.div``;

const DotsContainer = styled.div`
  margin-right: 150px;
  ${media.lessThan("large")`
    width: 50%;
    margin-right: 0;
    margin-bottom: 25px;
  `}
`;

const Skills = () => {
  return (
    <SkillsContainer>
      <h3>Skills & Abililties</h3>
      <SkillsAndAbilities>
        <ApplicationsContainer>
          {applications &&
            applications.map(application => (
              <Application application={application}></Application>
            ))}
        </ApplicationsContainer>
        <DotsContainer>
          {applications &&
            applications.map(application => <Dot application={application} />)}
        </DotsContainer>
        <AbilitiesContainer>
          {attributes &&
            attributes.map(attribute => <Attribute attribute={attribute} />)}
          <StyledResumeLink>
            <a href={resume} target="_blank">
              download full resume
            </a>
          </StyledResumeLink>
        </AbilitiesContainer>
      </SkillsAndAbilities>
    </SkillsContainer>
  );
};

export default Skills;
