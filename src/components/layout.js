/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react";
import PropTypes from "prop-types";
import { ThemeProvider } from "styled-components";
import Helmet from "react-helmet";

import Header from "./header";
import GlobalStyle from "../assets/globalStyle";
import Footer from "./Footer";
import theme from "../assets/theme";
import favicon from "../assets/images/favicon.png";

const Layout = ({ children, isWhite }) => {
  return (
    <ThemeProvider theme={theme}>
      <Helmet
        title="Taylor Bryant"
        link={[{ rel: "icon", type: "image/ping", href: `${favicon}` }]}
        meta={[
          {
            name: "application-name",
            content: "taylorbryant.design",
          },
          {
            name: "apple-mobile-web-app-title",
            content: "taylorbryant.design",
          },
          {
            name: "description",
            content:
              "A humble collection of work from Taylor Bryant, a UI/UX Designer based out of Charlotte, NC.",
          },
          {
            name: "keywords",
            content:
              "UI, UX, designer, web, design, strategy, creative, technology, apps, html, css",
          },
          {
            property: "og:site_name",
            content: "Taylor Bryant",
          },
          {
            property: "og:title",
            content: "Taylor Bryant",
          },
          {
            property: "og:type",
            content: "company",
          },
          {
            property: "og:image",
            content: "https://i.imgur.com/fzLmZE0.jpg",
          },
          {
            property: "og:url",
            content: "https://taylorbryant.design",
          },
          {
            property: "og:description",
            content:
              "A humble collection of work from Taylor Bryant, a UI/UX Designer based out of Charlotte, NC.",
          },
          {
            name: "robots",
            content: "index, nofollow",
          },
          {
            name: "language",
            content: "English",
          },
          {
            name: "twitter:card",
            content: "summary",
          },
          {
            name: "twitter:description",
            content:
              "A humble collection of work from Taylor Bryant, a UI/UX Designer based out of Charlotte, NC.",
          },
          {
            name: "viewport",
            content:
              "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0",
          },
        ]}
      />
      <>
        <GlobalStyle theme={theme}></GlobalStyle>
        <Header siteTitle="Taylor Bryant" isWhite={isWhite} />
        <main>{children}</main>
        <Footer></Footer>
      </>
    </ThemeProvider>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
