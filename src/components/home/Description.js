import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import media from "styled-media-query";
import xIcon from "../../assets/images/Group.png";
import david from "../../assets/images/davidsketch.png";

const StyledContainer = styled.div`
  padding: 150px 130px;
  position: relative;
  ${media.lessThan("large")`
    padding: 60px 15px;
  `}
`;

const StyledHeader = styled.h3`
  position: relative;
  text-transform: uppercase;
  font-size: 42px;
  max-width: 600px;
  ${media.lessThan("large")`
    width: 100%;
    font-size: 32px;
  `}
  &:after {
    content: "";
    display: inline-block;
    background-color: ${props => props.theme.green};
    height: 14px;
    width: 100%;
    position: absolute;
    bottom: 18px;
    left: -52%;
    z-index: -1;
    ${media.lessThan("large")`
      bottom: 12px;
      left: -15%;
  `}
  }
`;
const StyledText = styled.p`
  font-family: ${props => props.theme.fonts.secondary};
  font-size: 18px;
  line-height: 24px;
  max-width: 640px;
  margin: 40px auto;
  a {
    font-size: 20px;
    position: relative;
    font-family: ${props => props.theme.fonts.secondary};
    font-weight: 700;
    color: black;
    &:after {
      content: "";
      display: inline-block;
      background-color: ${props => props.theme.green};
      height: 14px;
      width: 100%;
      position: absolute;
      bottom: 5px;
      left: 0;
      z-index: -1;
    }
  }
`;

const DecorationsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  top: 50%;
  right: 50px;
  ${media.lessThan("large")`
    top: initial;
    bottom: 0;
    right: -50px;
  `}
  .david {
    max-width: 175px;
    margin-top: 50px;
  }
  .x-icon {
    max-width: 50px;
    ${media.lessThan("large")`
      display: none;
  `}
  }
  &:after {
    content: "";
    background-color: ${props => props.theme.green};
    display: inline-block;
    position: absolute;
    height: 605px;
    width: 4px;
    top: -670px;
    ${media.lessThan("large")`
      display: none;
  `}
  }
`;

const Description = () => {
  return (
    <StyledContainer>
      <StyledHeader>
        bringing ideas to life with stunning user interfaces.
      </StyledHeader>
      <StyledText>
        With experience in every step of the creative process, I pride myself on
        my ability to take a project from conception to completion. Each project
        I work on is a new opportunity to let my passion drive a unique and
        useful user experience. I love working on creative teams with product
        owners, developers and other designers. Details and organization are my
        thing, so if you're looking for someone to crush your next project, I
        think I'd be a great addition to your team. <br />
        <br />
        Feel free to check out my work below. Like what you see? <br /> <br />
        <a href="#contact">let's chat →</a>
      </StyledText>
      <DecorationsContainer>
        <img src={xIcon} alt="" className="x-icon" />
        <img src={david} alt="" className="david" />
      </DecorationsContainer>
    </StyledContainer>
  );
};

export default Description;
