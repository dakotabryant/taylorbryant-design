import React from "react";
import styled from "styled-components";
import media from "styled-media-query";
import bgImage from "../../assets/images/hero-back.jpg";
import frontImage from "../../assets/images/hero-front.png";

const HeroContainer = styled.section`
  height: 100vh;
  max-height: calc(100vw / 1.5);
  width: 100%;
  background-image: url(${bgImage});
  background-size: cover;
  background-repeat: no-repeat;
  display: flex;
  align-items: center;
  transition: ease-in-out 1s all;
  ${media.lessThan("large")`
    align-items: flex-end;
    max-height: initial;
    background-position-x: 92%;
  `}
`;

const FrontImage = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  background-size: cover;
  height: 100vh;
  max-height: calc(100vw / 1.5);
  background-image: url(${frontImage});
  width: 100%;
  height: 100%;
  height: 100vh;
  max-height: calc(100vw / 1.5);
  transition: ease-in-out 1s all;
  ${media.lessThan("large")`
    display: none;
  `}
`;

const HeroText = styled.h1`
  font-size: 11vw;
  font-weight: 800;
  font-family: ${props => props.theme.fonts.primary};
  text-transform: uppercase;
  line-height: 10vw;
  color: white;
  padding-left: 50px;
  span {
    color: ${props => props.theme.green};
  }
  ${media.lessThan("large")`
    padding-left: 15px;
    padding-bottom: 15px;
    font-size: 15vw;
    line-height: 15vw;
  `}
`;

const Hero = () => {
  return (
    <>
      <HeroContainer>
        <HeroText>
          Driven By <br />
          Beauty. <br />{" "}
          <span>
            Born to <br />
            create.
          </span>
        </HeroText>
      </HeroContainer>
      <FrontImage></FrontImage>
    </>
  );
};

export default Hero;
