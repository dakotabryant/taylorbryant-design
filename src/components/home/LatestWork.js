import React, { useState } from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import media from "styled-media-query";
import scribble from "../../assets/images/scribble.png";
import tiles from "../../constants/projects";

const WorkContainer = styled.section`
  width: 100%;
  padding: 170px 150px;
  text-align: center;
  ${media.lessThan("large")`
    padding: 0;
    padding-top: 60px;
  `}
  a {
    color: black;
    font-family: ${props => props.theme.fonts.secondary};
    font-weight: 700;
    position: relative;
    &::before {
      z-index: -1;
      display: inline-block;
      position: absolute;
      top: 40%;
      left: 0;
      content: "";
      width: 100%;
      height: 10px;
      background-color: ${props => props.theme.green};
    }
  }
  .view-all {
    margin-top: 25px;
    font-size: 22px;
  }
`;

const WorkHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;
  a {
    left: 150px;
    margin-top: 25px;
  }
  ${media.lessThan("large")`
    flex-direction: column;
    align-items: center;
    text-align: center;
  `}
  h3 {
    font-size: 42px;
    text-transform: uppercase;
    background-image: url(${scribble});
    background-size: contain;
    background-repeat: no-repeat;
    display: inline-block;
    background-position: right;
  }
`;

const LinksContainer = styled.ul`
  display: flex;
  list-style-type: none;
  li {
    margin-left: 25px;
    font-family: ${props => props.theme.fonts.secondary};
    position: relative;
    cursor: pointer;
    ${media.lessThan("large")`
      margin-left: 0;
      margin-right: 25px;
  `}
    &:hover {
      &::before {
        z-index: -1;
        display: inline-block;
        position: absolute;
        top: 40%;
        left: 0;
        content: "";
        width: 100%;
        height: 10px;
        background-color: ${props => props.theme.green};
      }
    }
  }
`;

const WorkTiles = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding-bottom: 80px;
  a {
    position: initial;
  }
  ${media.lessThan("large")`
    padding: 0 15px;
  `}
`;

const Tile = styled.div`
  height: 380px;
  width: 100%;
  background-image: url(${props => props.project.image});
  background-size: cover;
  background-position: center;
  position: relative;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 380px;
  opacity: 0;
  background-color: rgba(0, 0, 0, 0.8);
  color: white;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 24px;
  font-weight: 700;
  display: flex;
  justify-content: center;
  text-align: center;
  align-items: center;
  text-transform: uppercase;
  padding: 50px;
  cursor: pointer;
  &:hover {
    opacity: 1;
  }
  ${media.lessThan("medium")`
  display: none;
  `}
`;

const StyledLink = styled(Link)`
  width: 33%;
  ${media.lessThan("large")`
    width: 100%;
  `}
`;

const LatestWork = () => {
  const [currentTab, setCurrentTab] = useState("all");
  return (
    <WorkContainer>
      <WorkHeader>
        <h3>Latest Work</h3>
        <LinksContainer>
          <li onClick={() => setCurrentTab("all")}>all</li>
          <li onClick={() => setCurrentTab("brand")}>brand</li>
          <li onClick={() => setCurrentTab("web")}>web</li>
          <li onClick={() => setCurrentTab("app")}>app</li>
        </LinksContainer>
      </WorkHeader>
      <WorkTiles>
        {tiles &&
          tiles
            .filter(tile =>
              currentTab === "all" ? tile : tile.filters.includes(currentTab)
            )
            .map((tile, index) =>
              index > 8 ? (
                ""
              ) : (
                <StyledLink key={tile.text} to="/project" state={tile}>
                  <Tile project={tile}>
                    <Overlay>{tile.text}</Overlay>
                  </Tile>
                </StyledLink>
              )
            )}
      </WorkTiles>
      <Link to="/work" className="view-all">
        view all projects
      </Link>
    </WorkContainer>
  );
};

export default LatestWork;
