import React, { useState, useEffect } from "react";
import styled from "styled-components";
import media from "styled-media-query";
import art from "../../assets/images/Rotating Text/art.png";
import beautifulthings from "../../assets/images/Rotating Text/beautifulthings.png";
import digitalmasterpieces from "../../assets/images/Rotating Text/digitalmasterpieces.png";
import experiences from "../../assets/images/Rotating Text/experiences.png";
import mobileapps from "../../assets/images/Rotating Text/mobileapps.png";
import visualidentities from "../../assets/images/Rotating Text/visualidentities.png";
import websites from "../../assets/images/Rotating Text/websites.png";

const imagesArray = [
  art,
  digitalmasterpieces,
  experiences,
  mobileapps,
  visualidentities,
  websites,
];

const StyledContainer = styled.div`
  min-height: 400px;
  padding-top: 60px;
  background-color: ${props => props.theme.green};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
  ${media.lessThan("large")`
    margin-top: 50px;
    min-height: 220px;
  `}
  h3 {
    font-size: 42px;
    line-height: 46px;
    text-transform: uppercase;
    position: absolute;
    top: 80px;
    ${media.lessThan("large")`
    font-size: 32px;
    top: 40px;
  `}
  }
  img {
    max-width: 1000px;
    max-height: 100px;
    ${media.lessThan("large")`
      max-width: 375px;
  `}
  }
`;

const Titles = () => {
  const [currentIndex, setIndex] = useState(0);
  useEffect(() => {
    const interval = setInterval(() => {
      if (currentIndex < 5) {
        setIndex(currentIndex + 1);
      } else {
        setIndex(0);
      }
    }, 500);
    return () => clearInterval(interval);
  }, [currentIndex]);
  return (
    <StyledContainer>
      <h3>A maker of</h3>
      <img src={imagesArray[currentIndex]} alt="" />
    </StyledContainer>
  );
};

export default Titles;
