export const applications = [
  {
    name: "Sketch",
    rating: 9,
  },
  {
    name: "Prototyping",
    rating: 8,
  },
  {
    name: "InVision",
    rating: 9,
  },
  {
    name: "Photoshop",
    rating: 10,
  },
  {
    name: "Illustrator",
    rating: 10,
  },
  {
    name: "InDesign",
    rating: 8,
  },
  {
    name: "Lightroom",
    rating: 9,
  },
  {
    name: "After Effects",
    rating: 5,
  },
  {
    name: "HTML/CSS",
    rating: 6,
  },
  {
    name: "Photography",
    rating: 7,
  },
  {
    name: "Microsoft Office",
    rating: 10,
  },
];

export const attributes = ['Creative Thinker', 'Organize', 'Team Player', 'Attentive to detail', 'Quick Learner', 'Consistent', 'Dependable', 'Deadline-driven', 'Energetic', 'Ability to work under pressure', 'Dedicated'];
