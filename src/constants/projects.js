import fitbit from "../assets/images/thumbnails/fitbit-thumb.jpg";
import flora from "../assets/images/thumbnails/flora–fauna-thumb.jpg";
import national from "../assets/images/thumbnails/national-park-thumb.jpg";
import noSwitch from "../assets/images/thumbnails/no-switch-thumb.jpg";
import ocoord from "../assets/images/thumbnails/occord-thumb.jpg";
import pair from "../assets/images/thumbnails/pair-peo-thumb.jpg";
import pure from "../assets/images/thumbnails/pure-surgery-thumb.jpg";
import realEstate from "../assets/images/thumbnails/real-estate-thumb.jpg";
import redLobster from "../assets/images/thumbnails/red-lobster-thumb.jpg";
import simple from "../assets/images/thumbnails/simple-fit-thumb.jpg";
import sports from "../assets/images/thumbnails/sports-platform-thumb.jpg";
import unghost from "../assets/images/thumbnails/unghost-me-thumb.jpg";
import whales from "../assets/images/thumbnails/whales-thumb.jpg";
import cede from "../assets/images/thumbnails/cede-thumb.jpg";
import trh from "../assets/images/thumbnails/trh-thumb.jpg";
import realtor from "../assets/images/thumbnails/realtor-thumb.jpg";
import sitehands from "../assets/images/thumbnails/sitehands-thumb-min.jpg";
import cedeHero from "../assets/images/cede/1-hero.jpg";
import cedeExplainer from "../assets/images/cede/2-Explainer.jpg";
import cedeDashboard from "../assets/images/cede/3-Dashboard.jpg";
import cedeFeed from "../assets/images/cede/4-Community-Feed.jpg";
import cedeGroups from "../assets/images/cede/5-eGroups.jpg";
import cedeMessages from "../assets/images/cede/6-Messages.jpg";
import cedeDirectory from "../assets/images/cede/7-Directory.jpg";
import cedeMinistry from "../assets/images/cede/8-Ministry-community.jpg";
import cedeEvents from "../assets/images/cede/9-Events.jpg";
import cedeAdmin from "../assets/images/cede/10-Admin.jpg";
import cedeSignin from "../assets/images/cede/11-SignIn-Registration.jpg";
import cedeThanks from "../assets/images/cede/12-thanks.jpg";
import fitbitMac from "../assets/images/fitbit/MacMockup-min.jpg";
import fitbitPhone from "../assets/images/fitbit/PhoneMockup-min.jpg";
import floraHero from "../assets/images/flora/1-hero-min.jpg";
import floraCleanser from "../assets/images/flora/2-cleanser-moisturizer-min.jpg";
import floraSalts from "../assets/images/flora/3-salts-handcream-min.jpg";
import trhHero from "../assets/images/trh/1-hero-min.jpg";
import trhType from "../assets/images/trh/2-type-min.jpg";
import trhHome from "../assets/images/trh/4-home-min.jpg";
import trhSubpages from "../assets/images/trh/5-subpages-min.jpg";
import trhThanks from "../assets/images/trh/6-thanks-min.jpg";
import nationalMock from "../assets/images/national/national-park-mockup.jpg";
import noSwitchHero from "../assets/images/no-switch/1-hero-min.jpg";
import noSwitchBranding from "../assets/images/no-switch/2-branding-min.jpg";
import noSwitchLayouts from "../assets/images/no-switch/3-layouts-min.jpg";
import noSwitchMobile from "../assets/images/no-switch/4-mobile-min.jpg";
import noSwitchThanks from "../assets/images/no-switch/5_thanks-min.jpg";
import ocoordHero from "../assets/images/ocoord/1-hero.jpg";
import ocoordBrand from "../assets/images/ocoord/2-brand _ web.jpg";
import pairHero from "../assets/images/pair/1-hero.jpg";
import pairScreens from "../assets/images/pair/2-all-screens.jpg";
import pairType from "../assets/images/pair/3-color-typography.jpg";
import pairLayouts from "../assets/images/pair/4-layouts.jpg";
import pureMock from "../assets/images/pure/1-mockup.jpg";
import pureScreens from "../assets/images/pure/2-screens.jpg";
import realEstateHero from "../assets/images/real-estate/1-hero-colors.jpg";
import realEstateGetStarted from "../assets/images/real-estate/2-getstarted-browse.jpg";
import realEstateChat from "../assets/images/real-estate/3-chat-accounts.jpg";
import redLobsterHero from "../assets/images/red-lobster/1-hero.jpg";
import redLobsterIterations from "../assets/images/red-lobster/2-iterations.jpg";
import redLobsterMockup from "../assets/images/red-lobster/3-mockup.jpg";
import simpleHero from "../assets/images/simple/1-hero.jpg";
import simpleOverview from "../assets/images/simple/2-overview.jpg";
import simpleColors from "../assets/images/simple/3-colors.jpg";
import simplePhones from "../assets/images/simple/4-phones.jpg";
import simpleSignup from "../assets/images/simple/5-signup.gif";
import simpleDashboard from "../assets/images/simple/6-dashboard.gif";
import simpleWorkouts from "../assets/images/simple/7-workouts.gif";
import simpleTimer from "../assets/images/simple/8-timer.jpg";
import simpleProfile from "../assets/images/simple/9-profile.jpg";
import simpleThanks from "../assets/images/simple/10-thanks.jpg";
import sportshero from "../assets/images/sports/1-hero.jpg";
import sportstopics from "../assets/images/sports/2-topics.jpg";
import sportsisometric from "../assets/images/sports/3-isometric.jpg";
import sportsmatchup from "../assets/images/sports/4-matchup.jpg";
import sportschat from "../assets/images/sports/5-chat.jpg";
import sportsuser from "../assets/images/sports/6-user.jpg";
import sportswebapp from "../assets/images/sports/7-webapp.jpg";
import unghostlogo from "../assets/images/unghost/1-logo-min.jpg";
import unghostlogoAlt from "../assets/images/unghost/2-logo-alternatives-min.jpg";
import unghostcolors from "../assets/images/unghost/3-colors-min.jpg";
import unghostmockup from "../assets/images/unghost/4-mockup-min.jpg";
import unghosttype from "../assets/images/unghost/5-type-min.jpg";
import unghostbusinesscards from "../assets/images/unghost/6-businesscards-min.jpg";
import unghosttoolkit from "../assets/images/unghost/7-toolkit-min.jpg";
import unghostsocial from "../assets/images/unghost/8-social-image-min.jpg";
import whaleshero from "../assets/images/whales/1-hero.jpg";
import whaleslayout from "../assets/images/whales/2-layout.jpg";
import whalesmobile from "../assets/images/whales/3-mobile.jpg";
import whalesthanks from "../assets/images/whales/4-thanks.jpg";
import realtorHero from "../assets/images/realtor/buy-felicia.jpg";
import sitehandsHero from "../assets/images/sitehands/1-Hero-min.jpg";
import sitehandscolors from "../assets/images/sitehands/2-colors-typography-min.jpg";
import sitehandsHome from "../assets/images/sitehands/3-Home-min.jpg";
import sitehandsscreens from "../assets/images/sitehands/4-screens-min.jpg";
import sitehandsOnDemand from "../assets/images/sitehands/5-OnDemand-min.jpg";
import sitehandsPartners from "../assets/images/sitehands/6-Partners-Enterprise-min.jpg";
import sitehandsother from "../assets/images/sitehands/7-other-screens-min.jpg";
import sitehandsMobile from "../assets/images/sitehands/8-Mobile-min.jpg";
import sitehandsThanks from "../assets/images/sitehands/9-thanks-min.jpg";
import college1 from "../assets/images/college/Desktop1.png";
import college2 from "../assets/images/college/Desktop2.png";
import college3 from "../assets/images/college/Desktop3.png";
import college4 from "../assets/images/college/Desktop4.png";
import college5 from "../assets/images/college/Desktop5.png";
import college6 from "../assets/images/college/Desktop6.png";
import collegeThumb from "../assets/images/thumbnails/Lantern-thumb.png";
import marketplace1 from "../assets/images/marketplace/1.png";
import marketplace2 from "../assets/images/marketplace/2.png";
import marketplace3 from "../assets/images/marketplace/3.png";
import bootcampThumb from "../assets/images/thumbnails/bootcamp-thumb.png";
import comparison1 from "../assets/images/comparison/1.png";
import comparison2 from "../assets/images/comparison/2.png";
import comparison3 from "../assets/images/comparison/3.png";
import comparison4 from "../assets/images/comparison/4.png";
import comparison5 from "../assets/images/comparison/5.png";
import comparisonThumb from "../assets/images/thumbnails/compare-thumb.png";
import lanternThumb from "../assets/images/thumbnails/lantern-product-thumb.png";
import lantern1 from "../assets/images/lantern/1.png";
import lantern2 from "../assets/images/lantern/2.png";
import lantern3 from "../assets/images/lantern/3.png";
import lantern4 from "../assets/images/lantern/4.png";
import lantern5 from "../assets/images/lantern/5.png";
import lantern6 from "../assets/images/lantern/6.png";
import lantern7 from "../assets/images/lantern/7.png";
import lantern8 from "../assets/images/lantern/8.png";
import lantern9 from "../assets/images/lantern/9.png";
export default [
  {
    text: "Lantern product launch",
    filters: ["app", "web"],
    image: lanternThumb,
    projectImages: [
      lantern1,
      lantern2,
      lantern3,
      lantern4,
      lantern5,
      lantern6,
      lantern7,
      lantern8,
      lantern9,
    ],
  },
  {
    text: "Affordability Calculator",
    filters: ["app", "web"],
    image: collegeThumb,
    projectImages: [college1, college2, college3, college4, college5, college6],
  },
  {
    text: "School Comparison Tool",
    filters: ["web"],
    image: comparisonThumb,
    projectImages: [
      comparison1,
      comparison2,
      comparison3,
      comparison4,
      comparison5,
    ],
  },
  {
    text: "Bootcamp Question Flow",
    filters: ["app", "web"],
    image: bootcampThumb,
    projectImages: [marketplace1, marketplace2, marketplace3],
  },
  {
    text: "Real Estate IOS App",
    filters: ["app"],
    image: realEstate,
    projectImages: [realEstateHero, realEstateGetStarted, realEstateChat],
  },
  {
    text: "Fitness Identity & Website",
    filters: ["brand", "web"],
    image: noSwitch,
    projectImages: [
      noSwitchHero,
      noSwitchBranding,
      noSwitchLayouts,
      noSwitchMobile,
      noSwitchThanks,
    ],
  },
  {
    text: "Sports Chaplain Web Application",
    filters: ["app", "web"],
    image: cede,
    projectImages: [
      cedeHero,
      cedeExplainer,
      cedeDashboard,
      cedeFeed,
      cedeGroups,
      cedeMessages,
      cedeDirectory,
      cedeMinistry,
      cedeEvents,
      cedeAdmin,
      cedeSignin,
      cedeThanks,
    ],
  },
  {
    text: "Surgery Marketplace Dashboard",
    filters: ["app", "web", "brand"],
    image: pure,
    projectImages: [pureMock, pureScreens],
  },
  {
    text: "Pair PEO Brand Identity & Website",
    filters: ["brand", "web"],
    image: pair,
    projectImages: [pairHero, pairScreens, pairType, pairLayouts],
  },
  {
    text: "Ocoord Brand Identity & Website",
    filters: ["brand", "web"],
    image: ocoord,
    projectImages: [ocoordHero, ocoordBrand],
  },
  {
    text: "Sports Platform iOS App",
    filters: ["app"],
    image: sports,
    projectImages: [
      sportshero,
      sportstopics,
      sportsisometric,
      sportsmatchup,
      sportschat,
      sportsuser,
      sportswebapp,
    ],
  },
  {
    text: "Museum Exhibit Web Concept",
    filters: ["web"],
    image: whales,
    projectImages: [whaleshero, whaleslayout, whalesmobile, whalesthanks],
  },
  {
    text: "National Park Service Homepage Concept",
    filters: ["web"],
    image: national,
    projectImages: [nationalMock],
  },
  {
    text: "Marketing Agency Website",
    filters: ["brand", "web"],
    image: trh,
    projectImages: [trhHero, trhType, trhHome, trhSubpages, trhThanks],
  },
  {
    text: "UnghostMe Brand Identity",
    filters: ["brand"],
    image: unghost,
    projectImages: [
      unghostlogo,
      unghostlogoAlt,
      unghostcolors,
      unghostmockup,
      unghosttype,
      unghostbusinesscards,
      unghosttoolkit,
      unghostsocial,
    ],
  },
  {
    text: "Sitehands Website Redesign",
    filters: ["web", "app"],
    image: sitehands,
    projectImages: [
      sitehandsHero,
      sitehandscolors,
      sitehandsHome,
      sitehandsscreens,
      sitehandsOnDemand,
      sitehandsPartners,
      sitehandsother,
      sitehandsMobile,
      sitehandsThanks,
    ],
  },
  {
    text: "Red Lobster Brand Identity Concepts",
    filters: ["brand"],
    image: redLobster,
    projectImages: [redLobsterHero, redLobsterIterations, redLobsterMockup],
  },
  {
    text: "Realtor Website",
    filters: ["brand", "web"],
    image: realtor,
    projectImages: [realtorHero],
  },
  {
    text: "SimpleFit App",
    filters: ["brand", "app"],
    image: simple,
    projectImages: [
      simpleHero,
      simpleOverview,
      simpleColors,
      simplePhones,
      simpleSignup,
      simpleDashboard,
      simpleWorkouts,
      simpleTimer,
      simpleProfile,
      simpleThanks,
    ],
  },
  {
    text: "Fitbit Product Card Concept",
    filters: ["web"],
    image: fitbit,
    projectImages: [fitbitMac, fitbitPhone],
  },
  {
    text: "Flora & Fauna Packaging Design",
    filters: ["brand"],
    image: flora,
    projectImages: [floraHero, floraCleanser, floraSalts],
  },
];
