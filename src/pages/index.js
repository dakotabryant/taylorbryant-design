import React, { Component } from "react";
import Layout from "../components/layout";
import Hero from "../components/home/Hero";
import Description from "../components/home/Description";
import Titles from "../components/home/Titles";
import LatestWork from "../components/home/LatestWork";

class Index extends Component {
  render() {
    return (
      <Layout isWhite>
        <Hero></Hero>
        <Description></Description>
        <Titles></Titles>
        <LatestWork></LatestWork>
      </Layout>
    );
  }
}

export default Index;
