import React, { Component } from "react";
import Layout from "../components/layout";
import Hero from "../components/about/Hero";
import Experience from "../components/about/Experience";
import Skills from "../components/about/Skills";

class About extends Component {
  render() {
    return (
      <Layout>
        <Hero></Hero>
        <Experience></Experience>
        <Skills></Skills>
      </Layout>
    );
  }
}

export default About;
