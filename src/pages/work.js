import React, { useState } from "react";
import Layout from "../components/layout";
import { Link } from "gatsby";
import styled from "styled-components";
import media from "styled-media-query";
import tiles from "../constants/projects";
import man from "../assets/images/man.png";

const WorkContainer = styled.section`
  width: 100%;
  padding: 500px 150px;
  position: relative;
  padding-bottom: 100px;
  h2 {
    font-size: 240px;
    color: #efefef;
    position: absolute;
    top: 100px;
    left: -30px;
    ${media.lessThan("large")`
      top: 80px;
      left: 50%;
      -webkit-transform: translateX(-50%);
      transform: translateX(-50%);
      font-size: 120px;
  `}
  }
  img {
    position: absolute;
    width: 620px;
    right: -100px;
    top: 240px;
    ${media.lessThan("large")`
    display: none;
  `}
  }
  ${media.lessThan("large")`
    padding: 0;
    padding-top: 60px;
  `}
`;

const WorkHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;
  ${media.lessThan("large")`
    flex-direction: column;
    align-items: center;
    text-align: center;
    margin-top: 200px;
  `}
`;

const LinksContainer = styled.ul`
  display: flex;
  list-style-type: none;
  margin-block-start: 0;
  margin-block-end: 0;
  margin-inline-start: 0px;
  margin-inline-end: 0px;
  padding-inline-start: 0px;
  li {
    margin-left: 25px;
    font-family: ${props => props.theme.fonts.secondary};
    position: relative;
    cursor: pointer;
    &:first-of-type {
      margin-left: 0;
    }
    ${media.lessThan("large")`
      margin-left: 0;
      margin-right: 25px;
  `}
    &:hover {
      &::before {
        z-index: -1;
        display: inline-block;
        position: absolute;
        top: 40%;
        left: 0;
        content: "";
        width: 100%;
        height: 10px;
        background-color: ${props => props.theme.green};
      }
    }
  }
`;

const WorkTiles = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  ${media.lessThan("large")`
    padding: 0 15px;
  `}
`;

const Tile = styled.div`
  height: 380px;
  width: 33%;
  background-image: url(${props => props.project.image});
  background-size: cover;
  background-position: center;
  position: relative;
  ${media.lessThan("large")`
    width: 100%;
  `}
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  background-color: rgba(0, 0, 0, 0.8);
  color: white;
  font-family: ${props => props.theme.fonts.primary};
  font-size: 24px;
  font-weight: 700;
  display: flex;
  justify-content: center;
  text-align: center;
  align-items: center;
  text-transform: uppercase;
  padding: 50px;
  cursor: pointer;
  &:hover {
    opacity: 1;
  }
`;

const Work = () => {
  const [currentTab, setCurrentTab] = useState("all");
  return (
    <Layout>
      <WorkContainer>
        <h2>WORK</h2>
        <img src={man} alt="" />
        <WorkHeader>
          <LinksContainer>
            <li onClick={() => setCurrentTab("all")}>all</li>
            <li onClick={() => setCurrentTab("brand")}>brand</li>
            <li onClick={() => setCurrentTab("web")}>web</li>
            <li onClick={() => setCurrentTab("app")}>app</li>
          </LinksContainer>
        </WorkHeader>
        <WorkTiles>
          {tiles &&
            tiles
              .filter(tile =>
                currentTab === "all" ? tile : tile.filters.includes(currentTab)
              )
              .map((tile, index) => (
                <Tile key={tile.text} project={tile}>
                  <Link to="/project" state={tile}>
                    <Overlay>{tile.text}</Overlay>
                  </Link>
                </Tile>
              ))}
        </WorkTiles>
      </WorkContainer>
    </Layout>
  );
};

export default Work;
