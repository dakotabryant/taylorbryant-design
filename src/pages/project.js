import React, { Component } from "react";
import styled from "styled-components";
import media from "styled-media-query";
import Layout from "../components/layout";
import { TagsContainer } from "../components/about/Job";
import { Link, navigate } from "gatsby";

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Header = styled.div`
  margin-top: 80px;
  background-color: ${props => props.theme.green};
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;
  ${media.lessThan("large")`
    height: 135px;
    padding: 20px;
  `}
  h2 {
    font-size: 42px;
    ${media.lessThan("large")`
    font-size: 30px;
    text-align: center;
  `}
  }
`;

const SubHeader = styled.div`
  margin-bottom: 150px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  ${media.lessThan("large")`
    flex-direction: column-reverse;
    margin-bottom: 50px;
    margin-top: 40px;
  `}
  a {
    color: black;
    font-family: ${props => props.theme.fonts.secondary};
    font-weight: 700;
    position: absolute;
    left: 150px;
    ${media.lessThan("large")`
      position: relative;
      left: 0;
      font-size: 22px;
      margin-top: 40px;
  `}
    &::before {
      z-index: -1;
      display: inline-block;
      position: absolute;
      top: 40%;
      left: 0;
      content: "";
      width: 100%;
      height: 10px;
      background-color: ${props => props.theme.green};
    }
  }
`;

const ImageContainer = styled.img`
  width: calc(100% - 300px);
  max-width: 1140px;
  ${media.lessThan("large")`
    width: calc(100% - 40px);
  `}
`;

const StyledTag = styled(TagsContainer)`
  .box {
    text-align: center;
    min-width: 195px;
  }
`;

class Project extends Component {
  render() {
    if (
      typeof window !== "undefined" &&
      (!this.props.location || !this.props.location.state)
    ) {
      navigate("/work");
      return "";
    }
    if (typeof window === "undefined") return "";
    const { filters, text, projectImages } = this.props.location.state;
    return (
      <Layout>
        <Header>
          <h2>{text}</h2>
        </Header>
        <SubHeader>
          <Link to="/work">back to work</Link>
          <StyledTag>
            {filters &&
              filters.map(tag => (
                <div className="box">
                  <p>{tag}</p>
                </div>
              ))}
          </StyledTag>
        </SubHeader>
        <Wrapper>
          {projectImages &&
            projectImages.map((image, index) => (
              <ImageContainer key={index} src={image}></ImageContainer>
            ))}
        </Wrapper>
      </Layout>
    );
  }
}

export default Project;
